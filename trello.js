/**
 * Created by tnorberg on 4/10/2017.
 */

var Trello      = require("node-trello");
var Converter   = require('./lib/converter');
var Generator   = require('./lib/datagenerator');
var path        = require('path');
var fs          = require('fs');
// read settings
global.settings = require(path.join(process.cwd(), './settings'));

var options    = require('optimist')
    .usage('Generate release notes from Trello cards.\n\nUsage: $0')
    .alias('g', 'generate')
    .describe('g', 'Generate from list names')
    .string('g')
    .alias('b', 'boardName')
    .describe('b', 'The board names from Trello')
    .string('b')
    .alias('bs', 'board-show')
    .describe('bs', 'Show list of boards for current user')
    .string('bs')
    .alias('ls', 'list-show')
    .describe('ls', 'Show lists of given board name')
    .string('ls')
    .alias('cs', 'card-show')
    .describe('cs', 'Show list of cards for given list and board name')
    .string('cs')
    .alias('v', 'version')
    .describe('v', 'Product version')
    .string('v');
var optionArgs = options.argv;

var listName   = optionArgs.g;
var boardName  = optionArgs.b;
var showBoards = optionArgs.bs;
var showLists  = optionArgs.ls;
var showCards  = optionArgs.cs;
var version    = optionArgs.v;

var t = new Trello(settings.applicationKey, settings.userToken);

fs.exists(settings.exportPath, function (exists) {
    if (exists) {
        settings.root = settings.exportPath;
    }
    else {
        settings.root = path.join(process.cwd().replace(/\/+$/, ""), "export");

        exists = fs.existsSync(settings.root);
        if (!exists) {
            fs.mkdirSync(settings.root, 0666);
        }
    }
});

if (version) {
    if (version.length) {
        console.log("Taking other version than configured ...");
        settings.strings.version_number = version;
    }
    else {
        console.log("Option for version defined, but no version given. Taking from settings.");
    }
}

if (boardName) {
    if (boardName.length) {
        console.log("Taking other board name than configured ...");
        settings.boardName = boardName.split(',');
    }
    else {
        console.log("Option for board name defined, but no name given. Taking from settings.");
    }
}

if (listName) {
    if (listName.length) {
        console.log("Taking other list name than configured ...");
        settings.listName = listName.split(',');
    }
    else {
        console.log("Option for list name defined, but no list name given. Taking from settings.");
    }
}

if (typeof showLists !== 'undefined' || typeof showBoards !== 'undefined' || typeof showCards !== 'undefined') {
    if (showBoards === '') {
        start(true, false, false);
    }
    if (showLists === '') {
        start(false, true, false);
    }
    if (showCards === '') {
        start(false, false, true);
    }
}
else if (settings.listName instanceof Array) {
    start(false, false, false);
}
else if (listName === '' && !listName.length) {
    //Run when settings are ok
    start(false, false, false);
}
else {
    options.showHelp();
    example();
}

function example() {
    console.log('Example:');
    console.log('    index.js -g MyList');
    console.log('    index.js -g "Version 2.9, Version 3.0"');
    console.log('    index.js -g "Version 2.9, Version 3.0" -b "the board name"');
}

function start(showBoards, showLists, showCards) {
    t.get("/1/member/me/boards", function (err, boards) {
        if (err) {
            throw err;
        }
        if (showBoards) {
            return showData(boards);
        }
        var boardExists = false;
        boards.forEach(function (board) {
            if (settings.boardName.indexOf(board.name) !== -1) {
                boardExists = true;
                getLists(board, function (err, lists) {
                    if (err) {
                        throw err;
                    }
                    if (showLists) {
                        return showData(lists);
                    }
                    lists.forEach(function (list) {
                        if (settings.listName.indexOf(list.name) !== -1) {
                            getCards(list, function (err, cards) {
                                if (err) {
                                    throw err;
                                }
                                if (showCards) {
                                    return showData(cards);
                                }
                                if (cards.length > 0) {
                                    var converter = new Converter(path.join(__dirname, "templates"));
                                    console.log("Starting export of", cards.length, "cards having release notes ...(" + board.name + ": " + list.name + ")");
                                    var data  = Generator.generateData(cards);
                                    data.list = list;
                                    converter.convert(data, settings.template, function (convertError, data) {
                                        if (convertError) {
                                            console.log(convertError instanceof Error ? convertError.message : convertError);
                                        }
                                        else {
                                            save(data, list);
                                        }
                                    });
                                }
                                else {
                                    console.error("No cards having release notes found.");
                                }
                            });
                        }
                    });
                });
            }
        });
        if (boardExists === false) {
            showData(boards);
            console.error("BoardName does not exist, please pick from above values.");
        }
    });
}

function showData(data) {
    data.forEach(function (item) {
        console.log("Name: " + item.name);
        console.log("ID: " + item.id);
    });
}

function getLists(board, callback) {
    t.get("/1/boards/" + board.id + "/lists", callback);
}

function getCards(list, callback) {
    t.get("/1/lists/" + list.id + "/cards", {'fields': 'name,desc,labels,url'}, callback);
}

function save(content, list) {
    var filename = path.join(settings.root ? settings.root : process.cwd(), settings.strings.product.replace(/ /g, '_') + "_"
        + settings.strings.version_number.replace(/\./gi, '_') + '_'
        + list.name.replace(/ /g, '_') + '.md');

    fs.writeFile(filename, content, function (err) {
        if (err) {
            throw err;
        }
        console.log("Release notes exported successfully");
    });
}
