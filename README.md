# trello-release-notes

> _Heavily modified from [trello-releasenotes](https://github.com/devtyr/trello-releasenotes)_

A trello release notes generator for node.js

## Installation

You can install it using `npm`.

	npm install trello-release-notes

### Obtain a Trello token

First, log in to Trello and open [Generate API Keys](https://trello.com/1/appKey/generate "Generate API Keys"). You'll receive an key to use in the next step.

Second, call https://trello.com/1/authorize?key=YOUR_KEY&name=trello-releasenotes&expiration=never&response_type=token to grant access for this application. Be sure to replace `YOUR_KEY` with the key received in the first step.

> For further information visit: [Getting a Token from a User](https://trello.com/docs/gettingstarted/index.html#getting-a-token-from-a-user "Getting a Token from a User")

Store the key from the first action in setting `applicationKey` of `settings.json` and the token received from the second step in `userToken`.

## Usage

There are some settings you can set up in `settings.json`:

	applicationKey		Insert your obtained application key from Trello to get access to it
	userToken			Define your user token you'll receive when obtaining an application ey
	boardName			Define a list or string of boards names you want to search for list cards
	listName            Define a list or string of list names you want to populate in the release
	template			Defines the template to use
	exportLinks			if true, links are written to the exported data; the default template is able to handle that
	exportPath			Set a path if release notes should be exported to a specific path

> `version` and `product` of `strings` are used to generate the filename. 

### How to obtain the board id

When you open your Trello account you get a list of boards, open one of it and the URL will be something like

	https://trello.com/board/boardname/identifier

Copy `identifier` and set this one as the `boardId` in `settings.json`. This is used to search for lists if there is no command line option that overrides that setting.

### Export

To export release notes start it like shown bellow:

	node trello.js 

**trello-release-notes** uses [Mu - a fast, streaming Node.js Mustache engine](https://github.com/raycmorgan/Mu) as the templating engine. Have a look at the [documentation](http://mustache.github.com/mustache.5.html) to get familiar with Mustache. A simple template (it's the default of trello-releasenotes):

    # {{header}} {{product}} ({{list.name}})
    {{version}} **{{version_number}}**
    
    {{generated}} **{{date}}**
    
    ## {{subheader}}
    
    {{#data}}
    {{#link}}
    ### [{{name}}]({{link}} "{{name}}") ###
    {{/link}}
    {{^link}}
    ### {{name}} ###
    {{/link}}
    {{#labels}} `{{name}}` {{/labels}}
    
    {{#releasenotes}}
    
    {{singleNote}}
     
    {{/releasenotes}}
    {{/data}}
    {{^data}}
    No release notes available!
    {{/data}}


As you are able to configure the used templates within `settings.json` you can add new templates easily. Otherwise feel free to change the existing ones.

This is the available structure for templating:

	{
		"header": "",
		"product": "",
		"version": "",
		"version_number": "",
		"generated": "",
		"date": "",
		"subheader": "",
		"data": 
			[
				{
					"name": "",
					"labels": "",
					"link": "",
					"releasenotes": 
						[
							{ "singleNote": "" }
						]
				}
			]
	}

`data` is an array of all found cards. `releasenotes` is an array of all release notes found for the card.